# Terraform Proxy Learning

Learning terraform-proxy application, maybe a simple health API

## Usage

### Environment Variables

* `LISTEM_PORT` - Port to listem on (default: `8080`)
* `APP_HOST` - Hostname of the app to foward request ot (default: `app`)
* `APP_PORT` - Port of the app to foward request to (default: `9000`)

