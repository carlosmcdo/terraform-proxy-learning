#!/bin/sh

set -e

envsubst < /et/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'
